#include "crypto.hpp"
#include "array.hpp"
#include "network.hpp"
#include "pacote.hpp"
#include "socket.hpp"

#include <iostream>
#include <iomanip>
#include <ostream>
#include <istream>
#include <unistd.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>

using namespace std;


union tamanho_pacote{
	int tamanho;
	byte TamByte[4];
};

typedef union tamanho_interno{
	int tamanho;
	byte TamByte[2];
}TamanhoInterno;

int main(int argc, char const *argv[]){
	int fd;


	array::array *ObjetoDescriptado;
	array::array *PackRegsStart;
	array::array *PackRegistered;
	array::array *pacote_auth_start;
	array::array *PackChallenge;
	array::array *KeySDescriptada;
	array::array *MDescriptada;
	array::array *cabecalho;
	array::array *TokenTDescriptado;
	array::array *PackObjeto;
	array::array *hash;
	array::array *ValeuRegistered;
	array::array *TokenADescriptado;
	array::array *PachAuthenticated;
	

	array::array *Id = array::create(8);
	
	Id -> data[0] = 0x3f;
	Id -> data[1] = 0xdf;
	Id -> data[2] = 0x08;
	Id -> data[3] = 0x6b;
	Id -> data[4] = 0x8a;
	Id -> data[5] = 0xdf;
	Id -> data[6] = 0x2c;
	Id -> data[7] = 0x25;

	
	array::array *IdObject = array::create(8);
	IdObject->data[0] = 0x01;     
	IdObject->data[1] = 0x02;     
	IdObject->data[2] = 0x03;     
	IdObject->data[3] = 0x04;     
	IdObject->data[4] = 0x05;     
	IdObject->data[5] = 0x06;     
	IdObject->data[6] = 0x07;     
	IdObject->data[7] = 0x08;     

	
	RSA *chave_privada_cliente = crypto::rsa_read_private_key_from_PEM("private.pem");
	RSA *chave_publica_servIdor  = crypto::rsa_read_public_key_from_PEM("server_pk.pem");

	array::array *Id_criptado = crypto::rsa_encrypt(Id, chave_publica_servIdor);

	Pacote *PackRequestRegistration;
	PackRequestRegistration = new Pacote(0xC0);
	Pacote *PackRegsStartAuth;
	Pacote *PackRegisterId;
	Pacote *PackRegisteredAuth;
	Pacote *PackRequestAuth;
	Pacote *PachToken;
	Pacote *PackAuthStartAuth;
	Pacote *PackRequestChallenge;
	Pacote *PackChallengeAuth;
	Pacote *PackAuthenticated;
	Pacote *PachAuthenticatedAuth;
	Pacote *PackRequestObject;
	Pacote *PackObjetoAuth;

	Socket *SocketRequestRegistration;

	union tamanho_pacote PackRegisteredUnion;
	union tamanho_pacote PAckAutoStartUnion;
	union tamanho_pacote PackChallengeUnion;
	union tamanho_pacote PachAuthenticatedUnion;
	union tamanho_pacote PackObjetoUnion;


	cout << "Iniciando a comunicação com o servIdor..." <<endl;

	fd = network::connect("45.55.185.4", 3000);
	if(fd < 0){
		cout << "Conexao falhou" << fd << endl;
	}else{
		cout << "Conexao estabelecida" << endl;
		

		cout << "PROTOCOLO DE REGISTRO" << endl;
		network::write(fd, PackRequestRegistration->getPacote());
		if((PackRegsStart = network::read(fd))== nullptr){
			cout << "Leitura nula" << endl;
		}else{
			PackRegsStartAuth = new Pacote(PackRegsStart, 0xC1);
			if(PackRegsStartAuth->getPacote() == NULL){
			}else{
			}
			
			PackRegisterId = new Pacote(0xC2, Id_criptado);
			sleep(2);
			printf("\n");
			network::write(fd, PackRegisterId->getPacote());
			
			union tamanho_pacote PackRegisteredUnion;
			if((cabecalho = network::read(fd, 4)) == nullptr){
			}else{
			
				PackRegisteredUnion.TamByte[0] = cabecalho->data[0];
				PackRegisteredUnion.TamByte[1] = cabecalho->data[1];
				PackRegisteredUnion.TamByte[2] = cabecalho->data[2];
				PackRegisteredUnion.TamByte[3] = cabecalho->data[3];
			
				if((PackRegistered = network::read(fd, PackRegisteredUnion.tamanho)) == nullptr){
					
				}else{
					cout << "Imprimindo conteudo do pacote recebido REGISTERED: " << *PackRegistered << endl;
					
					PackRegisteredAuth = new Pacote(PackRegistered, 0xC3);
					cout << "Pacote Registered " << *PackRegisteredAuth->getValue() << endl;
					array::array *chave_S = PackRegisteredAuth->getValue();
					
					KeySDescriptada = crypto::rsa_decrypt(chave_S, chave_privada_cliente);
			

				}
			}
		}
	}

	cout << "PROTOCOLO DE AUTENTICAÇÃO" << endl;
	PackRequestAuth = new Pacote(0xA0, Id_criptado);
	network::write(fd, PackRequestAuth->getPacote());

	array::destroy(cabecalho);
	if((cabecalho = network::read(fd, 4)) == nullptr){
	}else{
		PAckAutoStartUnion.TamByte[0] = cabecalho->data[0];
		PAckAutoStartUnion.TamByte[1] = cabecalho->data[1];
		PAckAutoStartUnion.TamByte[2] = cabecalho->data[2];
		PAckAutoStartUnion.TamByte[3] = cabecalho->data[3];
		if((pacote_auth_start = network::read(fd, PAckAutoStartUnion.tamanho)) == nullptr){
		}else{
			PackAuthStartAuth = new Pacote(pacote_auth_start, 0xA1);
			array::array *token_A = PackAuthStartAuth->getValue();
			TokenADescriptado = crypto::rsa_decrypt(token_A, chave_privada_cliente);
		}
		PackRequestChallenge = new Pacote(0xA2);
		network::write(fd, PackRequestChallenge->getPacote());

		array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
		}else{
			PackChallengeUnion.TamByte[0] = cabecalho->data[0];
			PackChallengeUnion.TamByte[1] = cabecalho->data[1];
			PackChallengeUnion.TamByte[2] = cabecalho->data[2];
			PackChallengeUnion.TamByte[3] = cabecalho->data[3];
			if((PackChallenge = network::read(fd, PackChallengeUnion.tamanho)) == nullptr){
			}else{
				PackChallengeAuth = new Pacote(PackChallenge, 0xA4);
				array::array *M_criptado = PackChallengeAuth->getValue();
				MDescriptada = crypto::aes_decrypt(M_criptado, TokenADescriptado, KeySDescriptada);
			}
		}
		PackAuthenticated = new Pacote(0xA5, MDescriptada);
		network::write(fd, PackAuthenticated->getPacote());
		array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
		}else{
			PachAuthenticatedUnion.TamByte[0] = cabecalho->data[0];
			PachAuthenticatedUnion.TamByte[1] = cabecalho->data[1];
			PachAuthenticatedUnion.TamByte[2] = cabecalho->data[2];
			PachAuthenticatedUnion.TamByte[3] = cabecalho->data[3];

			if((PachAuthenticated = network::read(fd, PachAuthenticatedUnion.tamanho)) == nullptr){
			}else{

				PachAuthenticatedAuth = new Pacote(PachAuthenticated, 0xA6);
				array::array *token_T = PachAuthenticatedAuth->getValue();
				cout << "Token t encriptado" << *token_T <<endl;
				TokenTDescriptado = crypto::aes_decrypt(token_T, TokenADescriptado, KeySDescriptada);
				cout << "Token t descriptado" << endl;
			}
		}
	cout << "PROTOCOLO DE REQUISICAO" << endl;
	cout << "Id do objeto"<< *IdObject << endl;
	cout << "Token T descriptado"<< *TokenTDescriptado << endl;
	cout << "Chave S descriptado"<< *KeySDescriptada << endl;
	array::array *IdObject_criptado = crypto::aes_encrypt(IdObject, TokenTDescriptado, KeySDescriptada);
	cout << "Objeto encriptado" << *IdObject_criptado << endl;
	PackRequestObject = new Pacote(0xB0, IdObject_criptado);
	network::write(fd, PackRequestObject->getPacote());
	array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
			cout << "Erro na leitura do pacote" << endl;
		}else{
			cout << "Cabecalho = " << *cabecalho << endl;
			PackObjetoUnion.TamByte[0] = cabecalho->data[0];
			PackObjetoUnion.TamByte[1] = cabecalho->data[1];
			PackObjetoUnion.TamByte[2] = cabecalho->data[2];
			PackObjetoUnion.TamByte[3] = cabecalho->data[3];

			if((PackObjeto = network::read(fd, PackObjetoUnion.tamanho)) == nullptr){
				cout << "Erro na leitura do pacote" << endl;	
			}else{
				cout << "Imprimindo conteudo do pacote recebIdo OBJETO: " << *PackObjeto << endl;

				PackObjetoAuth = new Pacote(PackObjeto, 0xB1);
				array::array *objeto_encriptado = PackObjetoAuth->getValue();

				ObjetoDescriptado = crypto::aes_decrypt(objeto_encriptado, TokenTDescriptado, KeySDescriptada);
			}
		}

	}		

	ofstream foto;
	foto.open ("doc/foto.jpg", ios::binary);
	if( !foto ){
		cout << "Erro ao gerar foto";
		return 1;
	}else{
		cout << "Foto sendo criada...\n" << endl;
		sleep(2);
		for (int i = 0; i < ((int)ObjetoDescriptado->length); i++){
			foto << ObjetoDescriptado->data[i];
		}

	}
	foto.close();


network::close(fd);

	return 0;
}
