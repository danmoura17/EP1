#ifndef PACKAGE_H
#define PACKAGE_H

#ifndef _NETWORK_HPP
	#include "network.hpp"
#endif
#ifndef _ARRAY_H
	#include "array.hpp"
#endif
#ifndef PACOTE_H
	#include "pacote.hpp"
#endif

#include <iostream>
#include <iomanip>
#include <ostream>
#include <unistd.h>
#include <locale.h>

using namespace std;

class Socket{
	private:
		array::array *package;
	public:
		/* #############################################
						construtores
		############################################# */									
		Socket(int fd, array::array *package);			/*construtor vazio (default)*/
		Socket(int fd, size_t lenght); 					/*construtor */
		/* #############################################
						inicio geters
		############################################# */
		void setPackage(array::array *package);
		array::array *getPackage();
};

#endif